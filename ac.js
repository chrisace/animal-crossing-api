// Variables
////////////////////////////////
const villagersEl = document.querySelector(".villagers");
let villagersData = [];
const visitorNames = document.querySelectorAll(".visitor-name");
const islandNames = document.querySelectorAll(".visitor-island");

// Section Functions
////////////////////////////////

// Utilities
////////////////////////////////

const utils = {
  getRandomInt(max) {
    // random number generator
    return Math.floor(Math.random() * Math.floor(max)) + 1;
  },
  checkCounter(counter, amount) {
    if (counter === amount) {
      window.setTimeout(() => {
        resolve("Success!");
      }, 50);
    }
  },
  getInputValue(formClass, storageName, arr) {
    const form = document.querySelector(`.${formClass}`);
    const text = form.querySelector("input");
    form.addEventListener("submit", (e) => {
      e.preventDefault();
      localStorage.setItem(storageName, text.value);
      form.closest(".ac-welcome__form").classList.add("complete");
      if (arr) arr.forEach((item) => (item.textContent = text.value));
    });
  },
};

function runVillagers() {
  const endpoint = "https://acnhapi.com/v1/villagers/";
  const max = 392;
  const maxVillagers = 10;
  const createIslandBtn = document.querySelector(".create-island");
  const villagers = villagersEl.querySelectorAll(".villager");
  const villagerIcons = villagersEl.querySelectorAll(".villager__icon");
  const villagerNames = villagersEl.querySelectorAll(".villager__name");
  const villagersTitle = document.querySelector(".ac-villagers__title");

  async function getAcVillagerData() {
    // reset
    resetVillagers();

    // generate new set for villager ids
    if (villagersData.length > 0) villagersData = [];
    const villagerIDs = new Set();

    // generate new ids from randomizer
    while (villagerIDs.size < maxVillagers) {
      villagerIDs.add(utils.getRandomInt(max));
    }

    // generate mega prmise for all villager IDs
    const villagerIDsArray = Array.from(villagerIDs);
    const villagerIDsData = villagerIDsArray.map((id) =>
      fetch(`${endpoint}${id}`).then((res) => res.json())
    );

    // Get Data from mega promise
    try {
      const villagerIDsResults = await Promise.all(villagerIDsData);
      villagersData = [...villagerIDsResults];
    } catch (error) {
      console.log(error);
      return;
    }

    return assignVillagerElements(villagersData);
  }

  // populate villager elements
  function assignVillagerElements(arr) {
    let counter = 0;
    arr.forEach((item, index) => {
      const clsName = `.villager--${index}`;
      const villagerEl = document.querySelector(clsName);
      const villagerIcon = villagerEl.querySelector(".villager__icon");
      const villagerName = villagerEl.querySelector(".villager__name");
      const {
        id,
        ["bubble-color"]: bubbleColor,
        ["text-color"]: textColor,
        icon_uri: icon,
        name: { ["name-USen"]: name },
      } = item;
      villagerEl.dataset.id = `${id}`;
      villagerEl.dataset.index = `${index}`;
      villagerEl.classList.add("visible");
      villagerIcon.src = `${icon}`;
      villagerIcon.onload = () => {
        counter++;
        villagerEl.removeAttribute("disabled");
        villagerIcon.nextElementSibling.classList.add("visible");
        villagerEl.setAttribute(
          "style",
          `--bubbleColor: ${bubbleColor}; --textColor: ${textColor};`
        );
        villagerIcon.classList.add("visible");
        villagerEl.style.transitionDelay = 0.25 * index + "s";
        villagerIcon.style.transitionDelay = 0.25 * index + "s";
        villagerIcon.alt = `${name}`;
        villagerName.style.transitionDelay = 0.25 * index + "s";
        villagerName.textContent = `${name}`;
        // if (index === (villagers.length - 1) && (counter === villagers.length)) {
        // TODO: figure out how to detect if all images loaded successfully, then call provideVillagerDetails
        if (index === villagers.length - 1) {
          villagersTitle.textContent = "Here are your villagers";
          provideVillagerDetails();
        }
      };
    });
  }

  function resetVillagers() {
    // reset if pressed again
    villagersTitle.textContent = "Loading Your Villagers";

    const visible = document.querySelectorAll(".visible");
    visible.forEach((item) => item.classList.remove("visible"));
    villagers.forEach((villager) => {
      villager.classList.add("loading");
      villager.setAttribute("style", "");
    });
    villagerIcons.forEach((villagerIcon) => {
      villagerIcon.removeAttribute("src");
      villagerIcon.setAttribute("alt", "???");
      villagerIcon.setAttribute("style", "");
    });
    villagerNames.forEach((villagerName) => {
      villagerName.textContent = "???";
      villagerName.setAttribute("style", "");
    });
  }

  createIslandBtn.addEventListener("click", getAcVillagerData);
}

function provideVillagerDetails() {
  const villagerDetails = document.querySelector(".villager-details");
  console.log("ready to provide");
  villagersEl.addEventListener("click", handleClick);

  function handleClick(e) {
    if (e.target.closest("button")) {
      const btn = e.target.closest("button");
      // console.log(e.target.closest('button'))
      // console.log(btn.dataset.index)
      console.log(villagersData[btn.dataset.index]);
      const {
        ["birthday-string"]: bday,
        ["catch-phrase"]: phrase,
        hobby,
        image_uri: image,
        personality,
        saying,
        species,
        ["text-color"]: textColor,
        ["bubble-color"]: bubbleColor,
        name: { ["name-USen"]: name },
      } = villagersData[btn.dataset.index];
      const actualPersonality =
        personality === "Uchi" ? "Sisterly" : personality;
      const html = `
      <figure><img src="${image}" alt="${name}" height="256" width="256" /></figure>
      <figcaption>
        <div class="villager-details__item villager-details__item--name">Name: <strong>${name}</strong></div>
        <div class="villager-details__item villager-details__item--birthday">Birthday: <strong>${bday}</strong></div>
        <div class="villager-details__item villager-details__item--species">Species: <strong>${species}</strong></div>
        <div class="villager-details__item villager-details__item--personality">Personality: <strong>${actualPersonality}</strong></div>
        <div class="villager-details__item villager-details__item--hobby">Hobby: <strong>${hobby}</strong></div>
        <div class="villager-details__item villager-details__item--phrase">Phrase: <strong>${phrase}</strong></div>
        <div class="villager-details__item villager-details__item--saying">Saying: <strong>${saying}</strong></div>
      </figcaption>
      `;
      villagerDetails.innerHTML = html;
    }
  }
}

function getVisitorDetails() {
  if (
    localStorage.getItem("visitorName") &&
    localStorage.getItem("islandName")
  ) {
    const welcomeForms = document.querySelectorAll(".ac-welcome__form");
    welcomeForms.forEach((form) => form.classList.add("d-none"));
    document.querySelector(".ac-welcome__new").classList.toggle("d-none");
    document.querySelector(".ac-welcome__returning").classList.toggle("d-none");
    if (visitorNames)
      visitorNames.forEach(
        (item) => (item.textContent = localStorage.getItem("visitorName"))
      );
    if (islandNames)
      islandNames.forEach(
        (item) => (item.textContent = localStorage.getItem("islandName"))
      );
  }
  utils.getInputValue("ac-welcome__form-el--name", "visitorName", visitorNames);
  utils.getInputValue("ac-welcome__form-el--island", "islandName", islandNames);
}

function lifestyles() {
  const lifestyles = document.querySelector(".ac-ls-select");
  const lifestyleBtns = lifestyles.querySelectorAll(".ac-ls-select__item");
  const lifestyleMain = document.querySelector(".ac-ls-main");
  const lifestyleMainImages = lifestyleMain.querySelectorAll("img");
  lifestyles.addEventListener("click", setActiveLifestyle);
  function setActiveLifestyle(e) {
    if (e.target.closest(".ac-ls-select__item")) {
      const btn = e.target.closest(".ac-ls-select__item");
      const btnIndex = btn.dataset.index;
      lifestyleBtns.forEach((btn) => btn.classList.remove("active"));
      btn.classList.toggle("active");
      // lifestyleMainImages.forEach(img => img.dataset.index === btnIndex ? img.classList.add('active')  img.src = img.dataset.src : img.classList.remove('active') )
      lifestyleMainImages.forEach((img) => {
        if (img.dataset.index === btnIndex) {
          if (!img.src) {
            img.src = img.dataset.src;
            img.onload = () => {
              img.classList.add("active");
            };
          } else {
            img.classList.add("active");
          }
        } else {
          img.classList.remove("active");
        }
      });
    }
  }
}

const welcome = document.querySelector(".ac-welcome");
let myFirstPromise = new Promise((resolve, reject) => {
  let counter = 0;

  loadImages("ac-welcome__bg");
  loadImages("nook");

  function checkCounter(amount) {
    if (counter === amount) {
      window.setTimeout(() => {
        resolve("Success!");
      }, 2000);
    }
  }

  function loadImages(className) {
    const img = document.querySelector(`.${className}`);
    img.onload = () => {
      console.log("loaded" + img);
      counter++;
      console.log(counter);
      checkCounter(2);
    };
  }
});

myFirstPromise.then((successMessage) => {
  console.log("Yay! " + successMessage);
  welcome.classList.add("loaded");
});

// Call Functions
////////////////////////////////
getVisitorDetails();
lifestyles();
runVillagers();
